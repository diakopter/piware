Introduction
============

## Hardware is growing ##

More specifically, hardware _acceleration_

 * Qualcomm Snapdragon 805 _SoCs_
    + DSP / Video decoding
    + Wifi / Bluetooth / 4G
 * Intel/AMD x86 extensions
    + AES-NI
    + RdRand
 * Xilinx Zynq (ARM + FPGA)
 * Parallela board (Zynq + cluster-on-chip)


## Hardware is harder ##

 * At least, harder than software
    + Mass production, less _updateable_
    + More effort into optimization

 * We already try to make software more principled
    + Hardware needs it even more

 * Hardware design rewards high correctness assurance


## Hardware design "status quo" ##

Myriad of languages for specific design tasks...

 * Simulation
    + SystemC, VHDL/Verilog
 * Synthesis
    + VHDL/Verilog, C/C++ (subsets)
 * Verification: SAT solvers / Theorem provers


## Hardware design "status quo"

The same situation in software is nowadays unimaginable

 * To "simulate" (interpret) your program, you use Haskell
 * For compilation to x86, use OCaml
    + Whether it compiles, depends **a lot** on the compiler
 * And translate everything into `Coq` for verification...


## Functional hardware DSLs ##

 * Solve **most** of the problems with multiple descriptions / manual translation
 * "Popular" example: Lava (Chalmers) (Ex.)
    + Description, simulation, testing in Haskell
    + Verification through external SAT solver
 * Also, Haskell types are not as strong as we want
    + \mintinline{haskell}{addN :: [Bit] -> [Bit] -> [Bit]}


## Dependent types for hardware ##
 * We use _Agda_

 * Better specification of sizing constraints
    + \AF{addN} \AY{:} \AY{(}\AB{n} \AY{:} \AD{ℕ}\AY{)} \AY{→} \AD{C} \AY{(}\AN{2} \AO{*} \AB{n}\AY{)} \AY{(}\AI{suc} \AY{(}\AN{2} \AO{*} \AB{n}\AY{)}\AY{)}

 * Rule out design mistakes

 * Correctness proofs in the same language as the model
