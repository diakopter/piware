# Planning


## What we already have


### Hardware description combinators

 * Circuits built by structural combination
     + Some constructors:
         - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{Gate}
         - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{seq}
         - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{par}
         - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{Plug}

### Hardware description combinators

 * Parameterized by _gate library_ and _atomic type_
    + Examples: **boolean**, arithmetic, crypto

 * Fundamental gates have their behaviour _specified_
    + Instead of "calculated"
    + VHDL for fundamental gates also _given_

### Functional semantics

 * Turns a circuit description into a _function_ over bit vectors
    + _Executable_
    + In contrast to previous work with _relational_ semantics

 * Allows for circuit _simulation_
    + Also works with sequential circuits
        - _Stream_ semantics
        - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{stream-semantics-sig}


## What is planned


### Compilation to VHDL

 * Work in progress
 * Will work towards generating _hierarchical_ VHDL (vs. _flat_)
    + Can result in changes to _core_ combinators
       - Ex: tree of `seqs` vs. `fold` constructor
    + Naming is a challenge

### Randomized/exhaustive testing

 * Similar to Haskell's QuickCheck
 * Special case: exhaustive testing = proof of correctness
    + For specific circuit size, not infinite family

### Big case study

 * Parallel prefix cirxuits
    + Some sequential variations?

